﻿--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.4
-- Dumped by pg_dump version 9.3.4
-- Started on 2015-12-22 12:03:52

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 174 (class 3079 OID 11750)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1966 (class 0 OID 0)
-- Dependencies: 174
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 173 (class 1259 OID 82177)
-- Name: USER; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "USER" (
    user_id serial NOT NULL,
    user_name character varying(25),
    password character varying(35),
    first_name character varying(30),
    last_name character varying(35),
    email character varying(30),
    phone integer,
    age integer,
    career character varying(25)
);


ALTER TABLE public."USER" OWNER TO postgres;

--
-- TOC entry 171 (class 1259 OID 82161)
-- Name: session; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE session (
    id_session serial NOT NULL,
    user_id integer,
    "PID" integer, 
    session_date date,
    start_time time without time zone,
    end_time time without time zone,
    activate boolean
);


ALTER TABLE public.session OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 82123)
-- Name: task; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE task (
    task_id serial NOT NULL,
    task_name character varying(30),
    task_description text,
    start_date date,
    end_date date
);


ALTER TABLE public.task OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 82169)
-- Name: task_score; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE task_score (
    user_id integer NOT NULL,
    task_id integer NOT NULL,
    date_presentation date,
    score integer
);


ALTER TABLE public.task_score OWNER TO postgres;

--
-- TOC entry 1838 (class 2606 OID 82165)
-- Name: pk_session; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY session
    ADD CONSTRAINT pk_session PRIMARY KEY (id_session);


--
-- TOC entry 1835 (class 2606 OID 82130)
-- Name: pk_task; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY task
    ADD CONSTRAINT pk_task PRIMARY KEY (task_id);


--
-- TOC entry 1842 (class 2606 OID 82173)
-- Name: pk_task_score; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY task_score
    ADD CONSTRAINT pk_task_score PRIMARY KEY (user_id, task_id);


--
-- TOC entry 1847 (class 2606 OID 82181)
-- Name: pk_user; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "USER"
    ADD CONSTRAINT pk_user PRIMARY KEY (user_id);


--
-- TOC entry 1843 (class 1259 OID 82175)
-- Name: relationship_1_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_1_fk ON task_score USING btree (user_id);


--
-- TOC entry 1844 (class 1259 OID 82176)
-- Name: relationship_2_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_2_fk ON task_score USING btree (task_id);


--
-- TOC entry 1839 (class 1259 OID 82167)
-- Name: relationship_3_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_3_fk ON session USING btree (user_id);


--
-- TOC entry 1840 (class 1259 OID 82166)
-- Name: session_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX session_pk ON session USING btree (id_session);


--
-- TOC entry 1836 (class 1259 OID 82168)
-- Name: task_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX task_pk ON task USING btree (task_id);


--
-- TOC entry 1845 (class 1259 OID 82174)
-- Name: task_score_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX task_score_pk ON task_score USING btree (user_id, task_id);


--
-- TOC entry 1848 (class 1259 OID 82182)
-- Name: user_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX user_pk ON "USER" USING btree (user_id);


--
-- TOC entry 1849 (class 2606 OID 82183)
-- Name: fk_session_relations_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY session
    ADD CONSTRAINT fk_session_relations_user FOREIGN KEY (user_id) REFERENCES "USER"(user_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 1851 (class 2606 OID 82193)
-- Name: fk_task_sco_relations_task; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY task_score
    ADD CONSTRAINT fk_task_sco_relations_task FOREIGN KEY (task_id) REFERENCES task(task_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 1850 (class 2606 OID 82188)
-- Name: fk_task_sco_relations_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY task_score
    ADD CONSTRAINT fk_task_sco_relations_user FOREIGN KEY (user_id) REFERENCES "USER"(user_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 1965 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-12-22 12:03:54

--
-- PostgreSQL database dump complete
--



--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.4
-- Dumped by pg_dump version 9.3.4
-- Started on 2015-12-22 12:04:24

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 1962 (class 0 OID 82177)
-- Dependencies: 173
-- Data for Name: USER; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "USER" (user_name, password, first_name, last_name, email, phone, age, career) VALUES ('jhona', 'jhona', 'jhonatan', 'sanchez', 'jhonaumss@gmail.com', 69493775, 21, 'Ing. Sistemas');
INSERT INTO "USER" (user_name, password, first_name, last_name, email, phone, age, career) VALUES ( 'griss', 'griss', 'griselda', 'fernandez', 'gfernadez@gmail.com', 67899435, 21, 'Ing. Sistemas');
INSERT INTO "USER" (user_name, password, first_name, last_name, email, phone, age, career) VALUES ( 'ale', 'ale', 'alejandra', 'perez', 'aperez@gmail.com', 32546790, 20, 'Ing. Sistemas');
INSERT INTO "USER" (user_name, password, first_name, last_name, email, phone, age, career) VALUES ('mauri', 'mauri', 'mauricio', 'perez', 'mperez@gmail.com', 45876512, 21, 'Ing. Sistemas');
INSERT INTO "USER" ( user_name, password, first_name, last_name, email, phone, age, career) VALUES ('denis', 'denis', 'denis', 'perez', 'dperez@gmail.com', 68303928, 20, 'Ing. Sistemas');
INSERT INTO "USER" (user_name, password, first_name, last_name, email, phone, age, career) VALUES ( 'anthony', 'anthony', 'anthony', 'fernandez', 'afernandez@gmail.com', 12458909, 21, 'Ing. Sistemas');
INSERT INTO "USER" (user_name, password, first_name, last_name, email, phone, age, career) VALUES ('julio', 'julio', 'julio', 'perez', 'jperez@gmail.com', 54981245, 21, 'Ing. Informatica');
INSERT INTO "USER" ( user_name, password, first_name, last_name, email, phone, age, career) VALUES ( 'gustavo', 'gustavo', 'gustavo', 'perez', 'gperez@gmail.com', 12345678, 20, 'Ing. Sistemas');
INSERT INTO "USER" ( user_name, password, first_name, last_name, email, phone, age, career) VALUES ( 'juan', 'juan', 'juan', 'perez', 'jperez@gmail.com', 23456789, 22, 'Ing. Informatica');
INSERT INTO "USER" ( user_name, password, first_name, last_name, email, phone, age, career) VALUES ( 'dani', 'dani', 'daniela', 'fernandez', 'dfernandez@gmail.com', 23769834, 20, 'Ing. Sistemas');


--
-- TOC entry 1960 (class 0 OID 82161)
-- Dependencies: 171
-- Data for Name: session; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 1959 (class 0 OID 82123)
-- Dependencies: 170
-- Data for Name: task; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO task (task_name, task_description, start_date, end_date) VALUES ( 'tarea del login', 'muchachos deben hacer un login de acuerdo a lo dictado en clase', '2015-07-15', '2015-07-22');
INSERT INTO task ( task_name, task_description, start_date, end_date) VALUES ( 'tarea 2', 'muchachos deben hacer un resumen de lo aprendido esta semana', '2015-01-03', '2015-01-08');
INSERT INTO task ( task_name, task_description, start_date, end_date) VALUES ('tarea 3', 'muchachos deben repasar lo aprendido esta semana', '2015-02-03', '2015-07-08');
INSERT INTO task ( task_name, task_description, start_date, end_date) VALUES ( 'tarea 4', 'muchachos envien sus presentaciones lo mas pronto posible', '2015-02-23', '2015-07-30');
INSERT INTO task ( task_name, task_description, start_date, end_date) VALUES ( 'tarea 5', 'esta tarea la deben entregar con caratula no lo olviden :) ', '2015-07-23', '2015-07-30');
INSERT INTO task ( task_name, task_description, start_date, end_date) VALUES ( 'tarea 6', 'muchachos la tarea es olvidar lo hecho esta semana', '2015-07-23', '2015-07-30');
INSERT INTO task ( task_name, task_description, start_date, end_date) VALUES ( 'tarea 7', 'muchachos deben hacer un resumen de lo aprendido esta semana', '2015-07-23', '2015-07-30');
INSERT INTO task ( task_name, task_description, start_date, end_date) VALUES ( 'tarea 8', 'no olvidar poner sus nombres en su resumen', '2015-07-23', '2015-07-30');
INSERT INTO task ( task_name, task_description, start_date, end_date) VALUES ( 'tarea 9', 'ultima chance de entregar la tarea 8', '2015-07-23', '2015-07-30');
INSERT INTO task ( task_name, task_description, start_date, end_date) VALUES ( 'tarea 10', 'muchachos deben hacer un resumen de lo aprendido la anterior semana', '2015-07-23', '2015-7-30');
INSERT INTO task ( task_name, task_description, start_date, end_date) VALUES ('tarea 11', 'copiar la tarea 1 (la del login) lo perdi', '2015-07-23', '2015-07-30');
INSERT INTO task ( task_name, task_description, start_date, end_date) VALUES ( 'tarea 12', 'revisen su base datos ejeje', '2015-07-23', '2015-07-30');
INSERT INTO task ( task_name, task_description, start_date, end_date) VALUES ('tarea 13', 'esta tarea es para la prueba', '2015-07-23', '2015-07-30');
INSERT INTO task ( task_name, task_description, start_date, end_date) VALUES ('tarea 14', 'los ultimos 5 en entregar tienen 10 puntos menos', '2015-07-23', '2015-07-30');
INSERT INTO task ( task_name, task_description, start_date, end_date) VALUES ( 'tarea 15', '!!!!!!ultima tarea suerte!!!!!!!!', '2015-07-23', '2015-07-30');


--
-- TOC entry 1961 (class 0 OID 82169)
-- Dependencies: 172
-- Data for Name: task_score; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO task_score (user_id, task_id, date_presentation, score) VALUES (1, 1, '2015-02-12', 56);
INSERT INTO task_score (user_id, task_id, date_presentation, score) VALUES (2, 2, '2015-02-12', 66);
INSERT INTO task_score (user_id, task_id, date_presentation, score) VALUES (3, 3, '2015-02-12', 36);
INSERT INTO task_score (user_id, task_id, date_presentation, score) VALUES (4, 4, '2015-02-12', 56);
INSERT INTO task_score (user_id, task_id, date_presentation, score) VALUES (5, 5, '2015-02-12', 53);
INSERT INTO task_score (user_id, task_id, date_presentation, score) VALUES (6, 6, '2015-02-12', 56);
INSERT INTO task_score (user_id, task_id, date_presentation, score) VALUES (7, 7, '2015-02-12', 67);
INSERT INTO task_score (user_id, task_id, date_presentation, score) VALUES (8, 8, '2015-02-12', 32);
INSERT INTO task_score (user_id, task_id, date_presentation, score) VALUES (9, 9, '2015-02-12', 52);
INSERT INTO task_score (user_id, task_id, date_presentation, score) VALUES (10, 10, '2015-02-12', 76);
INSERT INTO task_score (user_id, task_id, date_presentation, score) VALUES (1, 11, '2015-02-12', 47);
INSERT INTO task_score (user_id, task_id, date_presentation, score) VALUES (2, 12, '2015-02-12', 82);
INSERT INTO task_score (user_id, task_id, date_presentation, score) VALUES (3, 13, '2015-02-12', 62);


-- Completed on 2015-12-22 12:04:24

--
-- PostgreSQL database dump complete
--

-- funcion para recuperar las notas del usuario activo

CREATE OR REPLACE FUNCTION get_scores_by_user_id()
  RETURNS TABLE(task_name character varying, score integer) AS
$BODY$
BEGIN
	return query
		select task.task_name, s.score
		from (select task_score.task_id, task_score.score
			from task_score
			where task_score.user_id = get_current_user()) s, task
		where task.task_id = s.task_id;
END $BODY$
  LANGUAGE plpgsql;


--funcion para recuperar el id del usuario al verificar.

CREATE OR REPLACE FUNCTION verify_user(uname text,pass text) RETURNS SETOF int AS $$ 
BEGIN
	RETURN QUERY
		select user_id
		from "USER"
		where user_name = uname and password = pass;
END;
$$ LANGUAGE plpgsql;

-- funcion para crear una sesion 

CREATE OR REPLACE FUNCTION create_sesion(userId integer)
  RETURNS void AS
$BODY$ 
BEGIN
insert into session (user_id, "PID", session_date, start_time, activate)
values (userId, pg_backend_pid(), current_date, localtime, true);
END;
$BODY$
  LANGUAGE plpgsql;

-- funcion para cerrar la sesion activa

CREATE OR REPLACE FUNCTION close_sesion()
  RETURNS void AS
$BODY$ 
BEGIN
update session set activate = false, end_time = localtime
where "PID" = pg_backend_pid(); 
return;
END;
$BODY$
  LANGUAGE plpgsql;

-- funcion para obtener el usuario actual

CREATE OR REPLACE FUNCTION get_current_user()
  RETURNS integer AS
$BODY$ 
DECLARE
userId integer;
BEGIN
userId = (select user_id from session where "PID" = pg_backend_pid()):: integer;
return userId;
END;
$BODY$
  LANGUAGE plpgsql;

-- funcion para recuperar todas las tareas

CREATE OR REPLACE FUNCTION get_all_tasks()
  RETURNS TABLE(task_name character varying, task_description text, start_date date, end_date date) AS
$BODY$
BEGIN
	return query
		select task.task_name, task.task_description, task.start_date, task.end_date
		from task;
END $BODY$
  LANGUAGE plpgsql;

-- funcion para recuperar todos los usuarios

CREATE OR REPLACE FUNCTION get_users()
  RETURNS TABLE(user_id integer,
first_name character varying, 
last_name character varying, 
email character varying,
phone integer, 
age integer, 
career character varying) AS
$BODY$
BEGIN
	return query
		select "USER".user_id,
			"USER".first_name,
			"USER".last_name, 
			"USER".email, 
			"USER".phone, 
			"USER".age, 
			"USER".career
		from "USER";
END $BODY$
  LANGUAGE plpgsql;
CREATE TABLE schedules
(
  day character varying(9),
  hour character varying(15),
  classroom character varying(6)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE schedules
  OWNER TO postgres;
insert into schedules (day,hour,classroom) values('Martes','11:15-12:45','655');
insert into schedules (day,hour,classroom) values('Miercoles','11:15-12:45','655');
insert into schedules (day,hour,classroom) values('Viernes','06:45-08:15','690B');

-- Function: get_schedules()

-- DROP FUNCTION get_schedules();

CREATE OR REPLACE FUNCTION get_schedules()
  RETURNS TABLE(day character varying, hour character varying, classroom character varying) AS
$BODY$BEGIN
	return query
		select schedules.day, schedules.hour, schedules.classroom
		from schedules;
END $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 10;
ALTER FUNCTION get_schedules()
  OWNER TO postgres;  
