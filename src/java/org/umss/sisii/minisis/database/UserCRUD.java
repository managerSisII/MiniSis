/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.umss.sisii.minisis.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.umss.sisii.minisis.model.Schedule;
import org.umss.sisii.minisis.model.Score;
import org.umss.sisii.minisis.model.Task;
import org.umss.sisii.minisis.model.User;

/**
 *
 * @author PC_
 */
public class UserCRUD {
    
    private static final String GET_SCORE_BY_USER_ID
            = "select * from get_scores_by_user_id();";
    private static final String VERIFY_USER
            = "select verify_user('%s', '%s')";
    private static final String GET_ALL_TASKS
            = "select * from get_all_tasks()";
    private static final String CREATE_SESION
            = "select create_sesion(%d)";
    private static final String GET_USERS 
            = "select * from get_users();";
    private static final String GET_SCHEDULES
            = "select * from get_schedules()";
    private static final String CLOSE_SESION
            = "select close_sesion()";
    
    private final DBManager manager;
    private final Connection connection;

    public UserCRUD() {
        manager = DBManager.getInstance();
        connection = manager.getConnection();
    }
    
    public User checkUser(User user) {
        try {
            Statement statement = connection.createStatement();
            String query = String.format(VERIFY_USER, user.getUserName(), user.getUserPassword());
            ResultSet rs = statement.executeQuery(query);
            rs.next();
            user.setId(rs.getInt(1));
        } catch (SQLException ex) {
            System.out.println("Fallo en la verificacion");
        }
        return user;
    }

    public List<Task> getAllTasks() {
        List<Task> tasks = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(GET_ALL_TASKS);
            while (rs.next()) {
                Task task = new Task();
                task.setTaskName(rs.getString(1));
                task.setDescription(rs.getString(2));
                task.setStartDate(rs.getString(3));
                task.setEndDate(rs.getString(4));
                tasks.add(task);
            }
        } catch (SQLException ex) {
            System.out.println("Error al recuperar las tareas");
        }
        return tasks;
    }

    public List<Score> getScores() {
        List<Score> result = new ArrayList<>();
        Score current;
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(GET_SCORE_BY_USER_ID);
            while (rs.next()) {
                current = new Score();
                current.setTask(rs.getString(1));
                current.setScore(Integer.valueOf(rs.getString(2)));
                result.add(current);
            }
        } catch (SQLException ex) {
            System.out.println("Error al recuperar las notas");
        }
        return result;
    }

    public void createSesion(int userId) {
       try {
            Statement statement = connection.createStatement();
            String query = String.format(CREATE_SESION, userId);
            statement.executeQuery(query);
        } catch (SQLException ex) {
            System.out.println("Error al crear sesion");
        }
    }
    
    public List<User> getUsers() {
        List<User> result = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            User current;
            ResultSet rs = statement.executeQuery(GET_USERS);
            while(rs.next()) {
                current = new User();
                current.setId(rs.getInt(1));
                current.setFirstName(rs.getString(2));
                current.setLastName(rs.getString(3));
                current.setEmail(rs.getString(4));
                current.setPhone(rs.getInt(5));
                current.setAge(rs.getInt(6));
                current.setCareer(rs.getString(7));
                result.add(current);
            }
        } catch (SQLException ex) {
            System.out.println("Fallo al recuperar a los usuarios");
        }
        return result;
    }
    
    public List<Schedule> getSchedule() {
        List<Schedule> schedule = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(GET_SCHEDULES);
            while(rs.next()) {
                Schedule sch = new Schedule();
                sch.setDay(rs.getString(1));
                sch.setHour(rs.getString(2));
                sch.setClassroom(rs.getString(3));
                schedule.add(sch);
            }
        } catch (SQLException ex) {
            System.out.println("Error al recuperar los horarios");
        }
        return schedule;
    }
    
    public void closeSesion() {
        try {
            Statement statement = connection.createStatement();
            statement.executeQuery(CLOSE_SESION);
            manager.closeConnection();
        } catch (SQLException ex) {
            System.out.println("Error cerrando la sesión");
        }
    }
}
