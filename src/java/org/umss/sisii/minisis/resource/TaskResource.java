/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.umss.sisii.minisis.resource;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.umss.sisii.minisis.database.UserCRUD;
import org.umss.sisii.minisis.model.Score;
import org.umss.sisii.minisis.model.Task;
import org.umss.sisii.minisis.model.User;

/**
 *
 * @author PC_
 */
@Path("/task")
@Produces(MediaType.APPLICATION_JSON)
public class TaskResource {

    private static final UserCRUD CRUD = new UserCRUD();

    @POST
    public List<Score> getScores(User user) {
        List<Score> scores = CRUD.getScores();
        return scores;
    }
    
    @GET
    public List<Task> getAllTasks() {
        return CRUD.getAllTasks();
    }
}
