package org.umss.sisii.minisis.resource;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.umss.sisii.minisis.database.UserCRUD;
import org.umss.sisii.minisis.model.User;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Anthony
 */
@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {

    private static final UserCRUD CRUD = new UserCRUD();

    @POST
    public User checkUserExample(User user) {
        User result = CRUD.checkUser(user);
        int userId = result.getId();
        if(userId > 0)
            CRUD.createSesion(userId);
        return result;
    }

    @GET
    public List<User> getAllUsers(){
        return CRUD.getUsers();
    }
}
