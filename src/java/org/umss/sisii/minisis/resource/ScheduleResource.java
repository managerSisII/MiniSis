/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.umss.sisii.minisis.resource;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.umss.sisii.minisis.database.UserCRUD;
import org.umss.sisii.minisis.model.Schedule;

/**
 *
 * @author anthony
 */
@Path("/schedule")
@Produces(MediaType.APPLICATION_JSON)
public class ScheduleResource {
    
    private final UserCRUD userCRUD = new UserCRUD();
    
    @GET
    public List<Schedule> getSchedules() {
        return userCRUD.getSchedule();
    }
    
}
