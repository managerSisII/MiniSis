(function () {
    'use strict';
    
    var modules = [
        'ngNewRouter',
        'app.taskList',
        'app.minisiss',
        'app.taskScoreList',
        'app.tableData',
        'app.studentList',
        'app.tableSchedule'
    ];

    angular.module('app', modules)
           .controller('AppController', AppController);

    AppController.$inject = [
        '$router',
        'Minisiss'
    ];
    
    function AppController($router, Minisiss) {
        var vm = this;
        
        vm.closeSesion = function () {
            Minisiss.closeSesion();
        };
        
        $router.config([
            {
                path: '/', redirectTo: '/taskList'
            },
            {
                path: '/taskList', component: 'taskList'
            },
            {
                path: '/taskScoreList', component: 'taskScoreList'
            },
            {
                path: '/studentList', component: 'studentList'
            },
            {
                path:'/schedule', component: 'tableSchedule'
            }
        ]);
    }
})();


