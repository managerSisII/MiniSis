(function () {
    'use strict';

    angular
            .module('app.studentList', [])
            .controller('StudentListController', StudentListController);

    StudentListController.$inject = ['Minisiss'];

    function StudentListController(Minisiss) {
        var vm = this;
        vm.column;
        vm.users = [];

        Minisiss.getUsers(onSuccess, onError);

        function onSuccess(data) {
            var aux = data.data;
            var i;
            for (i = 0; i < aux.length; i++) {
                delete aux[i].id;
            }
            vm.users = aux;
            vm.column = Object.keys(vm.users[0]);
            console.log("Se recuperaron los usuarios");
        }

        function onError() {
            console.log("No se recuperaron los usuarios");
        }
    }
})();