(function() {
    'use strict';
    
    angular.module('app.tableData', [])
           .directive('tableData', TableData);
   
   function TableData() {
       return {
           restrict: 'E',
           scope: {
               column: '=',
               row: '=',
               title: '@?',
               subtitule: '@?'
           },
           templateUrl: 'components/table-data/table-data.html'
       };
   }
})();

