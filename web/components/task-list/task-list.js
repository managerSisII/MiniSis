(function () {
    'use strict';

    angular
        .module('app.taskList', [])
        .controller('TaskListController', TaskListController);

    TaskListController.$inject = ['Minisiss'];
    
    function TaskListController(Minisiss) {
        var vm = this;
        vm.column;
        vm.tasks = [];
        
        Minisiss.getTasks(onSuccess, onError);

        function onSuccess(data) {
            vm.tasks = data.data;
            vm.column = Object.keys(vm.tasks[0]);
            console.log("Se recuperaron las tareas");
            console.log(vm.tasks);
        };

        function onError() {
            console.log("No se recuperaron las tareas");
        };
    }
})();


