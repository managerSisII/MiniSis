(function () {
    'use strict';

    angular.module('app.taskScoreList', [])
           .controller('TaskScoreListController', TaskScoreListController);

    TaskScoreListController.$inject = ['Minisiss'];
    function TaskScoreListController(Minisiss) {
        var vm = this;
        vm.scores = [];
        vm.column;
        vm.userId = Minisiss.userId;
        var user = {
            id: vm.userId
        };
        Minisiss.getScores(user, onSuccess, onError);

        function onSuccess(data) {
            vm.scores = data.data;
            vm.column = Object.keys(vm.scores[0]);
        }

        function onError() {
        }
    }
})();