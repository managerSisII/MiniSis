(function () {
    'use strict';
    
    angular.module('login', ['app.minisiss']).controller('LoginController', LoginController);

    LoginController.$inject = ['$location', 'Minisiss'];
    
    function LoginController($location, Minisiss) {
        var vm = this;
        vm.userName;
        vm.userPassword;
        vm.userId;
        
        vm.verifyUser = function () {
            var user = {
                userName: vm.userName,
                userPassword: vm.userPassword
            };
            Minisiss.verifyUser(user, onSuccess, onError);
        };
        
        function onSuccess(data) {
            console.log("User Exist");
            console.log("... " + data.data.userName);
            console.log("... " + data.data.userPassword);
            console.log("... " + data.data.id);
            console.log(data);
            vm.userId = data.data.id;
            console.log(vm.userId);
            if (vm.userId > 0) {
//                $location.path('/index2.html');
                window.location.href = "index2.html";
            } else {
                alert('No estas registrado');
            }
        }
        
        function onError() {
            console.log("User not exist!");
        }
        
    }
})();


